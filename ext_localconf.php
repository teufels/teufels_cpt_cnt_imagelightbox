<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelslightboxgallery',
	array(
		'Gallery' => 'list',
		
	),
	// non-cacheable actions
	array(
		'Gallery' => '',
		
	)
);
