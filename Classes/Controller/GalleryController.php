<?php
namespace TEUFELS\TeufelsCptCntImagelightbox\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * GalleryController
 */
class GalleryController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * galleryRepository
	 *
	 * @var \TEUFELS\TeufelsCptCntImagelightbox\Domain\Repository\GalleryRepository
	 * @inject
	 */
	protected $galleryRepository = NULL;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$galleries = $this->galleryRepository->findAll();
		
		// FLEXFORMS config values
		$sFolderForGallery = explode(':', $this->settings['foldersource'])[1];
		$sThumbsPerRow = $this->settings['thumbamount'];
		
		$aImages = array();
		if ($sFolderForGallery != '') {

			$sFolder = 'fileadmin' . $sFolderForGallery;
			//auch komplette Pfade möglich ($sFolder = "download/files";)

			if (is_dir($sFolder)) {

				// Verzeichnis öffnen
				$open_dir = opendir($sFolder);

				$i = 0;
				while ($file = readdir($open_dir)) {
					// Verzeichnis auslesen
					// Abfrage: . und .. ausschliessen
					if ($file != '.' && $file != '..') {
						// Abfrage nach anzeigbaren Dateiendungen
						if (strstr($file, '.gif') || strstr($file, '.jpg') || strstr($file, '.jpeg') || strstr($file, '.png')) {

//							var_dump($file);

							$sImagePath = $_SERVER['DOCUMENT_ROOT'] . '/' .$sFolder .$file;
							$aImageSize = getimagesize($sImagePath);

							$aImages[$i]['path'] = $sFolder . $file;
							$aImages[$i]['width'] = $aImageSize[0];
							$aImages[$i]['height'] = $aImageSize[1];
						}
					}
					$i++;
				}
				closedir($open_dir);
			}

		}
		$this->view->assign('galleries', $galleries);
		$this->view->assign('aImages', $aImages);
		$this->view->assign('ahtml', $ahtml);
	}

}